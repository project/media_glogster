<?php

/**
 * @file media_glogster/includes/media_glogster.styles.inc
 * Styles definitions for Media: Glogster.
 */

/**
 * Implementation of Styles module hook_styles_register().
 */
function media_glogster_styles_register() {
  return array(
    'MediaGlogsterStyles' => array(
      'field_types' => 'file',
      'name' => t('MediaGlogster'),
      'description' => t('Media Glogster styles.'),
      'path' => drupal_get_path('module', 'media_glogster') .'/includes',
      'file' => 'media_glogster.styles.inc',
    ),
  );
}

/**
 *  Implements hook_styles_containers(). (Deprecated in version 2)
 */
function media_glogster_styles_containers() {
  return array(
    'file' => array(
      'containers' => array(
        'media_glogster' => array(
          'label' => t('Glogster Styles'),
          'data' => array(
            'streams' => array(
              'glogster',
            ),
            'mimetypes' => array(
              'video/glogster',
            ),
          ),
          'weight' => 0,
          'filter callback' => 'media_glogster_formatter_filter',
          'themes' => array(
            'field_formatter_styles' => 'media_glogster_field_formatter_styles',
            'styles' => 'media_glogster_styles',
            'preview' => 'media_glogster_preview_style',
          ),
          'description' => t('Glogster Styles will display embedded Glogster posters to your choosing, such as by resizing. You can !manage.', array('!manage' => l(t('manage your Glogster styles here'), 'admin/config/media/media-glogster-styles'))),
        ),
      ),
    ),
  );
}

function media_glogster_formatter_filter($variables) {
  if (isset($variables['object'])) {
    $object = $variables['object'];
    return (file_uri_scheme($object->uri) == 'glogster') && ($object->filemime == 'video/glogster');
  }
}

/**
 * Implementation of the File Styles module's hook_file_styles_filter().
 */
function media_glogster_file_styles_filter($object) {
  if ((file_uri_scheme($object->uri) == 'glogster') && ($object->filemime == 'video/glogster')) {
    return 'media_glogster';
  }
}

/**
 *  Implements hook_styles_styles().
 */
function media_glogster_styles_styles() {
  $styles = array(
    'file' => array(
      'containers' => array(
        'media_glogster' => array(
          'styles' => array(
            'glogster_preview' => array(
              'name' => 'glogster_preview',
              'effects' => array(
                array('label' => t('Resize'), 'name' => 'resize', 'data' => array('width' => 488, 'height' => 661)),
              ),
            ),
            'glogster_full' => array(
              'name' => 'glogster_full',
              'effects' => array(
                array('label' => t('Resize'), 'name' => 'resize', 'data' => array('width' => 488, 'height' => 661)),
              ),
            ),
          ),
        ),
      ),
    ),
  );

  return $styles;
}

/**
 *  Implements hook_styles_presets().
 */
function media_glogster_styles_presets() {
  $presets = array(
    'file' => array(
      'small' => array(
        'media_glogster' => array(
          'glogster_preview',
        ),
      ),
      'large' => array(
        'media_glogster' => array(
          'glogster_full',
        ),
      ),
      'original' => array(
        'media_glogster' => array(
          'glogster_full',
        ),
      ),
    ),
  );
  return $presets;
}

/**
 * Implementation of Styles module hook_styles_default_containers().
 */
function media_glogster_styles_default_containers() {
  // We append Glogster to the file containers.
  return array(
    'file' => array(
      'containers' => array(
        'media_glogster' => array(
          'class' => 'MediaGlogsterStyles',
          'name' => 'media_glogster',
          'label' => t('Glogster'),
          'preview' => 'media_glogster_preview_style',
        ),
      ),
    ),
  );
}


/**
 * Implementation of Styles module hook_styles_default_presets().
 */
function media_glogster_styles_default_presets() {
  $presets = array(
    'file' => array(
      'containers' => array(
        'media_glogster' => array(
          'default preset' => 'video',
          'styles' => array(
            'original' => array(
              'default preset' => 'video',
            ),
            'medium' => array(
              'default preset' => 'linked_medium',
            ),
            'large' => array(
              'default preset' => 'large_video',
            ),
          ),
          'presets' => array(
            'video' => array(
              array(
                'name' => 'video',
                'settings' => array(),
              ),
            ),
            'large_video' => array(
              array(
                'name' => 'resize',
                'settings' => array(
                  'width' => 488,
                  'height' => 661,
                ),
              ),
              array(
                'name' => 'video',
                'settings' => array(),
              ),
            ),
          ),
        ),
      ),
    ),
  );
  return $presets;
}
