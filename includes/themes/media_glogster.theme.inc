<?php

/**
 * @file media_glogster/includes/themes/media_glogster.theme.inc
 *
 * Theme and preprocess functions for Media: Glogster.
 */

/**
 * Preprocess function for theme('media_glogster_video').
 */
function media_glogster_preprocess_media_glogster_video(&$variables) {
  // Build the URL for display.
  $uri = $variables['uri'];
  $wrapper = file_stream_wrapper_get_instance_by_uri($uri);
  $parts = $wrapper->get_parameters();
  $variables['glog_id'] = check_plain($parts['glog_id']);

  $variables['width'] = isset($variables['width']) ? $variables['width'] : media_glogster_variable_get('width');
  $variables['height'] = isset($variables['height']) ? $variables['height'] : media_glogster_variable_get('height');
  $variables['scale'] = round((100 * $variables['width']) / 960);

  $variables['wrapper_id'] = 'media_glogster_' . $variables['glog_id'] . '_' . $variables['id'];

  // Add the media embed fluid sizing library.
  drupal_add_library('media', 'media_embed');
}

/**
 * Preview for Styles UI.
 */
function theme_media_glogster_preview_style($variables) {
  $variables['uri'] = media_glogster_variable_get('preview_uri');
  $variables['field_type'] = 'file';
  $variables['object'] = file_uri_to_object($variables['uri']);

  return theme('styles', $variables);
}
