<?php

/**
 * @file media_glogster/includes/themes/media-glogster-video.tpl.php
 *
 * Template file for theme('media_glogster_video').
 *
 * Variables available:
 *  $uri - The uri to the Glogster poster, such as glogster://glog_id/something.
 *  $glog_id - The unique identifier of the Glogster poster..
 *  $width - The width to render.
 *  $height - The height to render.
 *  $scale - The scale of the Glogster poster.
 */
?>
<div class="media-glogster-preview-wrapper media-embed-resize" id="<?php print $wrapper_id; ?>">
<iframe frameborder="0" height="<?php print $height; ?>" marginheight="0" marginwidth="0" scrolling="no" src="http://www.glogster.com/glog.php?glog_id=<?php print $glog_id; ?>&amp;scale=<?php print $scale; ?>" style="overflow: hidden;" width="<?php print $width; ?>"></iframe>
</div>
