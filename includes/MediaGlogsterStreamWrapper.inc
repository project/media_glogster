<?php

/**
 *  @file
 *  Create a Glogster StreamWrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $glogster = new ResourceGlogsterStreamWrapper('glogster://?glog_id=[glog_id]');
 */

class MediaGlogsterStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://edu.glogster.com/glog.php';

  function getTarget($f) {
    return FALSE;
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/glogster';
  }

  public function url_stat($uri, $flags) {
    $stat = parent::url_stat($uri, $flags);
    $stat[2] = $stat['mode'] = 0100444;
    return $stat;
  }
}
