<?php

/**
 * @file
 * Styles class implementation for Media: Glogster.
 */

class MediaGlogsterStyles extends FileStyles {
  function video($effect) {
    $variables = array(
      'uri' => $this->getUri(),
      'width' => $this->getWidth(),
      'height' => $this->getHeight(),
    );
    $this->setOutput(theme('media_glogster_video', $variables));
  }
}
