<?php

/**
 * @file media_glogster/media_glogster.module
 *
 * Media: Glogster provides a stream wrapper and formatters for interactive
 * posters provided by Glogster, available at http://glogster.com/.
 */

// A registry of variable_get defaults.
include_once('includes/media_glogster.variables.inc');

// Hooks and callbacks for integrating with Styles module for display.
include_once('includes/media_glogster.styles.inc');

// Hooks and callbacks for integrating with File Entity module for display.
include_once('includes/media_glogster.formatters.inc');

/**
 * Implements hook_media_internet_providers().
 */
function media_glogster_media_internet_providers() {
  $path = drupal_get_path('module', 'media_glogster');
  return array(
    'MediaInternetGlogsterHandler' => array(
      'title' => 'glogster',
    ),
  );
}

/**
 * Implements hook_stream_wrappers().
 */
function media_glogster_stream_wrappers() {
  return array(
    'glogster' => array(
      'name' => t('Glogster posters'),
      'class' => 'MediaGlogsterStreamWrapper',
      'description' => t('Interactive posters provided by Glogster.'),
      'type' => STREAM_WRAPPERS_READ_VISIBLE,
    ),
  );
}

/**
 * Implements hook_theme().
 */
function media_glogster_theme($existing, $type, $theme, $path) {
  return array(
    'media_glogster_video' => array(
      'variables' => array('uri' => NULL, 'width' => NULL, 'height' => NULL),
      'file' => 'media_glogster.theme.inc',
      'path' => $path . '/includes/themes',
      'template' => 'media-glogster-video',
    ),
  );
}

/**
 * Implements hook_media_parse().
 */
function media_glogster_media_parse($embed_code) {
  $handler = new MediaInternetGlogsterHandler($embed_code);
  return $handler->parse($embed_code);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function media_glogster_form_media_edit_alter(&$form, &$form_state) {
  $media = $form_state['build_info']['args'][0];
  if ($media->filemime == 'video/glogster' && isset($form['preview'][0])) {
    if (!isset($form['preview'][0]['#suffix'])) {
      $form['preview'][0]['#suffix'] = '';
    }
    $url = drupal_realpath($media->uri);
    $form['preview'][0]['#suffix'] .= l($url, $url);
  }
}

/**
 * Implements hook_media_format_form_prepare_alter().
 */
function media_glogster_media_format_form_prepare_alter(&$form, &$form_state, $media) {
  $settings = array('autosubmit' => ($media->type == "video"));
  drupal_add_js(array('media_format_form' => $settings), 'setting');
}

/**
 * Implements hook_ctools_plugin_api().
 */
function media_glogster_ctools_plugin_api($owner, $api) {
  static $api_versions = array(
    'file_entity' => array(
      'file_default_displays' => 1,
    ),
  );
  if (isset($api_versions[$owner][$api])) {
    return array('version' => $api_versions[$owner][$api]);
  }
}
